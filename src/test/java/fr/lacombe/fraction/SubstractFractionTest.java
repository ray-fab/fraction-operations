package fr.lacombe.fraction;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class SubstractFractionTest {
	
	@Test
	void testValidFractionSubstractAnotherValidFractionWithSameDenominators() {
		Fraction leftFraction = SimpleFraction.builder().numerator(2).denominator(5).build();
		Fraction rigthFraction = SimpleFraction.builder().numerator(1).denominator(5).build();
		Fraction substractFraction = leftFraction.substract(rigthFraction);
		assertEquals(1,substractFraction.getNumerator(),  "Numerator must 1");
		assertEquals(5, substractFraction.getDenominator(), "Numerator must 5");
	}
	
	@Test
	void testValidFractionSubstractAnotherValidFractionWithDifferentDenominators() {
		Fraction leftFraction = SimpleFraction.builder().numerator(2).denominator(5).build();
		Fraction rigthFraction = SimpleFraction.builder().numerator(2).denominator(3).build();
		Fraction substractFraction = leftFraction.substract(rigthFraction);
		assertEquals(-4, substractFraction.getNumerator(), "Numerator must -4");
		assertEquals(15, substractFraction.getDenominator(), "Numerator must 15");
	}
	
	@Test
	void testValidFractionSubstractAnotherValidFractionAndSimplifly() {
		Fraction leftFraction = SimpleFraction.builder().numerator(7).denominator(10).build();
		Fraction rigthFraction = SimpleFraction.builder().numerator(1).denominator(10).build();
		Fraction substractFraction = leftFraction.substract(rigthFraction);
		assertNotEquals(6, substractFraction.getNumerator(), "Must be factorise");
		assertNotEquals(10, substractFraction.getDenominator(), "Must be factorise");
		assertEquals(3, substractFraction.getNumerator(), "Numerator must 4");
		assertEquals(5, substractFraction.getDenominator(), "Numerator must 5");
	}
	
	@Test
	void testInvalidFractionSubstractInvalidFraction() {
		Fraction substractFraction = InvalidFraction.of().substract(InvalidFraction.builder().build());
		assertTrue(substractFraction instanceof InvalidFraction, "Invalid Fraction substract Invalid Fraction must be Invalid Fraction");
	}
	
	@Test
	void testInvalidFractionSubstractNullFraction() {
		Fraction substractFraction = InvalidFraction.of().substract(NullFraction.of());
		assertTrue(substractFraction instanceof InvalidFraction, "Invalid Fraction substract Null Fraction must be Invalid Fraction");
	}

	@Test
	void testInvalidFractionSubstractValidFraction() {
		Fraction invalidFraction = InvalidFraction.of();
		Fraction otherFraction = SimpleFraction.builder().numerator(1).denominator(5).build();
		Fraction substractFraction = invalidFraction.substract(otherFraction);
		assertTrue(substractFraction instanceof InvalidFraction, "Invalid Fraction substract other Fraction must be Invalid Fraction");
	}
	
	@Test
	void testNullFractionSubstractNullFraction() {
		Fraction substractFraction = NullFraction.of().substract(NullFraction.of());
		assertTrue(substractFraction instanceof NullFraction, "Null Fraction substract Null Fraction must be Null Fraction");
	}

	@Test
	void testNullFractionSubstractValidFraction() {
		Fraction nullFraction = NullFraction.of();
		Fraction otherFraction = SimpleFraction.builder().numerator(1).denominator(5).build();
		Fraction substractFraction = nullFraction.substract(otherFraction);
		assertEquals(otherFraction, substractFraction.opposite(), "Null Fraction substract other Fraction must be opposite of other");
	}
	
	@Test
	void testNullFractionSubstractInvalidFraction() {
		Fraction substractFraction = NullFraction.of().substract(InvalidFraction.builder().build());
		assertTrue(substractFraction instanceof InvalidFraction, "Null Fraction substract Invalid Fraction must be Invalid Fraction");
	}

}
