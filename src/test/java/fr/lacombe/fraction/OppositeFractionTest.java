package fr.lacombe.fraction;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class OppositeFractionTest {
	
	@Test
	void testOppositeValidFraction() {
		Fraction fraction = SimpleFraction.builder().numerator(2).denominator(5).build();
		Fraction opposite = fraction.opposite();
		assertEquals(fraction.getNumerator(), -1 *opposite.getNumerator(), "Numerator must become negative");
		assertEquals(fraction.getDenominator(), opposite.getDenominator(), "Denominator must not be changed");
	}
	
	@Test
	void testOppositeInvalidFraction() {
		Fraction fraction = SimpleFraction.builder().numerator(2).denominator(0).build();
		Fraction opposite = fraction.opposite();
		assertTrue(opposite instanceof InvalidFraction, "Numerator must become negative");
	}
	
	@Test
	void testOppositeNullFraction() {
		Fraction fraction = SimpleFraction.builder().numerator(0).denominator(1).build();
		Fraction opposite = fraction.opposite();
		assertTrue(opposite instanceof NullFraction, "Opposite of Null Fraction must be Null Fraction");
	}

}
