package fr.lacombe.fraction;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class GetResultFractionTest {
	
	@Test
	void testValidFractionGetResult() {
		double result = SimpleFraction.builder().numerator(3).denominator(12).build().getResult();
		assertEquals(0.25, result);
	}
	
	@Test
	void testInvalidGetResult() {
		assertThrows(UnsupportedOperationException.class, InvalidFraction.of(SimpleFraction.builder().numerator(1).build())::getResult);
	}
	
	@Test
	void testNullFractionGetResult() {
		double result = SimpleFraction.builder().numerator(0).denominator(12).build().getResult();
		assertEquals(0, result);
	}

}
