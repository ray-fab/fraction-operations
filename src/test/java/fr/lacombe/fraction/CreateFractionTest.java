package fr.lacombe.fraction;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class CreateFractionTest {
	

	@Test
	void testCreateValidFraction() {
		Fraction divideFraction = SimpleFraction.builder().numerator(1).denominator(2).build();
		assertTrue(divideFraction instanceof SimpleFraction);
	}
	
	@Test
	void testCreateInvalidFraction() {
		Fraction divideFraction = SimpleFraction.builder().numerator(1).denominator(0).build();
		assertTrue(divideFraction instanceof InvalidFraction, "Zero denominator must be Invalid Fraction");
	}
	
	@Test
	void testCreateNullFraction() {
		Fraction divideFraction = SimpleFraction.builder().numerator(0).denominator(2).build();
		assertTrue(divideFraction instanceof NullFraction, "Zero numerator must be Null Fraction");
	}

}
