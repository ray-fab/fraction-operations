package fr.lacombe.fraction;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class GreaterThanTest {
	
	@Test
	void testValidFractionIsGreaterThanAnotherValidFraction() {
		Fraction leftFraction = SimpleFraction.builder().numerator(5).denominator(7).build();
		Fraction rigthFraction = SimpleFraction.builder().numerator(3).denominator(7).build();
		assertTrue(leftFraction.isGreaterThan(rigthFraction), "Left fraction is greater than rigth fraction");
	}
	
	@Test
	void testValidFractionIsNotGreaterThanAnotherValidFraction() {
		Fraction leftFraction = SimpleFraction.builder().numerator(1).denominator(5).build();
		Fraction rigthFraction = SimpleFraction.builder().numerator(2).denominator(5).build();
		assertFalse(leftFraction.isGreaterThan(rigthFraction), "leftFraction is not greater than rigthFraction");
	}
	
	@Test
	void testValidFractionIsNotGreaterThanInvalidFraction() {
		Fraction leftFraction = SimpleFraction.builder().numerator(1).denominator(5).build();
		Fraction rigthFraction = InvalidFraction.of();
		assertFalse(leftFraction.isGreaterThan(rigthFraction), "Valid Fraction is not greater than Invalid Fraction");
	}
	
	@Test
	void testValidFractionIsGreaterThanNullFraction() {
		Fraction leftFraction = SimpleFraction.builder().numerator(1).denominator(5).build();
		Fraction rigthFraction = NullFraction.of();
		assertTrue(leftFraction.isGreaterThan(rigthFraction), "Valid Fraction is always greater than Null Fraction");
	}
	
	@Test
	void testValidFractionIsNotGreaterThanNullFraction() {
		Fraction leftFraction = SimpleFraction.builder().numerator(-1).denominator(5).build();
		Fraction rigthFraction = NullFraction.of();
		assertFalse(leftFraction.isGreaterThan(rigthFraction), "Valid Fraction is always greater than Null Fraction");
	}
	
	@Test
	void testNullFractionIsGreaterThanNullFraction() {
		assertFalse(NullFraction.of().isGreaterThan(NullFraction.of()), "Null Fraction always is less than other");
	}
	
	@Test
	void testNullFractionIsGreaterThanValidFraction() {
		assertFalse(NullFraction.of().isGreaterThan(SimpleFraction.builder().numerator(2).denominator(5).build()), "Null Fraction always is less than other");
	}
	
	@Test
	void testNullFractionIsNotGreaterThanValidFraction() {
		assertTrue(NullFraction.of().isGreaterThan(SimpleFraction.builder().numerator(-2).denominator(5).build()), "Null Fraction always is less than other");
	}
	
	@Test
	void testInvalidFractionIsGreaterThanInvalid() {
		assertFalse(InvalidFraction.of().isGreaterThan(InvalidFraction.of()), "Invalid Fraction always is less than other");
	}
	
	@Test
	void testInvalidFractionIsGreaterThanNullFraction() {
		assertFalse(InvalidFraction.of().isGreaterThan(NullFraction.of()), "Invalid Fraction always is less than other");
	}
	
	@Test
	void testInvalidFractionIsGreaterThanValidFraction() {
		assertFalse(InvalidFraction.of().isGreaterThan(SimpleFraction.builder().numerator(1).denominator(5).build()), "Invalid Fraction always is less than other");
	}
	
	
	
}
