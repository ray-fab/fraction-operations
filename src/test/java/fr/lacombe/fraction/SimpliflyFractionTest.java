package fr.lacombe.fraction;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class SimpliflyFractionTest {
	@Test
	void testValidFractionSimplifly() {
		Fraction fraction = SimpleFraction.builder().numerator(4).denominator(12).build();
		Fraction simplify = fraction.simplifly();
		assertEquals(3, simplify.getDenominator(), "Numerator must 3");
		assertEquals(1, simplify.getNumerator(), "Numerator must 1");
		assertEquals(0, simplify.compareTo(fraction), "Simplifly must be equals than fraction");
		assertTrue(simplify.isEquals(fraction), "Simplifly must be equals than fraction");
	}
	
	@Test
	void testInvalidFractionSimplifly() {
		Fraction simplify = InvalidFraction.of().simplifly();
		assertTrue(simplify instanceof InvalidFraction, "Simplifly Invalid Fraction must be Invalid fraction");
	}
	
	@Test
	void testNullFractionSimplifly() {
		Fraction simplify = NullFraction.of().simplifly();
		assertTrue(simplify instanceof NullFraction, "Simplifly Null Fraction must be Null fraction");
	}
}
