package fr.lacombe.fraction;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class LestThanFractionTest {
	
	@Test
	void testValidFractionIsLestThanValidFraction() {
		Fraction leftFraction = SimpleFraction.builder().numerator(1).denominator(5).build();
		Fraction rigthFraction = SimpleFraction.builder().numerator(2).denominator(3).build();
		assertTrue(leftFraction.isLessThan(rigthFraction), "leftFraction is not greater than rigthFraction");
	}
	
	@Test
	void testValidFractionIsNotLestThanValidFraction() {
		Fraction leftFraction = SimpleFraction.builder().numerator(2).denominator(5).build();
		Fraction rigthFraction = SimpleFraction.builder().numerator(1).denominator(5).build();
		assertFalse(leftFraction.isLessThan(rigthFraction), "leftFraction is not greater than rigthFraction");
	}
}
