package fr.lacombe.fraction;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class AddFractionTest {
	
	@Test
	void testValidFractionAddAnotherValidFractionWithSameDenominators() {
		Fraction leftFraction = SimpleFraction.builder().numerator(2).denominator(5).build();
		Fraction rigthFraction = SimpleFraction.builder().numerator(1).denominator(5).build();
		Fraction addFraction = leftFraction.add(rigthFraction);
		assertEquals(3, addFraction.getNumerator(), "Numerator must 3");
		assertEquals(5, addFraction.getDenominator(), "Numerator must 5");
	}
	
	@Test
	void testValidFractionAddAnotherValidFractionWithDifferentDenominators() {
		Fraction leftFraction = SimpleFraction.builder().numerator(2).denominator(5).build();
		Fraction rigthFraction = SimpleFraction.builder().numerator(2).denominator(3).build();
		Fraction addFraction = leftFraction.add(rigthFraction);
		assertEquals(16, addFraction.getNumerator(), "Numerator must 16");
		assertEquals(15, addFraction.getDenominator(), "Numerator must 15");
	}
	
	@Test
	void testValidFractionAddAnotherValidFractionAndSimplifly() {
		Fraction leftFraction = SimpleFraction.builder().numerator(1).denominator(10).build();
		Fraction rigthFraction = SimpleFraction.builder().numerator(7).denominator(10).build();
		Fraction addFraction = leftFraction.add(rigthFraction);
		assertNotEquals(8, addFraction.getNumerator(), "Must be simplifly");
		assertNotEquals(10, addFraction.getDenominator(), "Must be simplifly");
		assertEquals(4, addFraction.getNumerator(), "Numerator must 4");
		assertEquals(5, addFraction.getDenominator(), "Numerator must 5");
	}
	
	@Test
	void testValidFractionAddNegativeValidFraction() {
		Fraction leftFraction = SimpleFraction.builder().numerator(1).denominator(10).build();
		Fraction rigthFraction = SimpleFraction.builder().numerator(-7).denominator(10).build();
		Fraction addFraction = leftFraction.add(rigthFraction);
		assertEquals(-3, addFraction.getNumerator(), "Numerator must -3");
		assertEquals(5, addFraction.getDenominator(), "Numerator must 5");
	}
	
	@Test
	void testNegativeValidFractionAddNegativeValidFraction() {
		Fraction leftFraction = SimpleFraction.builder().numerator(-1).denominator(10).build();
		Fraction rigthFraction = SimpleFraction.builder().numerator(-7).denominator(10).build();
		Fraction addFraction = leftFraction.add(rigthFraction);
		assertEquals(-4, addFraction.getNumerator(), "Numerator must -4");
		assertEquals(5, addFraction.getDenominator(), "Numerator must 5");
	}
	
	@Test
	void testValidFractionAddNullFraction() {
		Fraction fraction = SimpleFraction.builder().numerator(1).denominator(10).build();
		Fraction nullFraction = NullFraction.of();
		Fraction addFraction = fraction.add(nullFraction);
		assertEquals(fraction, addFraction, "Add Null Fraction must be left fraction");
	}
	
	@Test
	void testValidFractionAddInvalidInvalid() {
		Fraction fraction = SimpleFraction.builder().numerator(1).denominator(10).build();
		Fraction nullFraction = InvalidFraction.of();
		Fraction addFraction = fraction.add(nullFraction);
		assertTrue(addFraction instanceof InvalidFraction, "Add Invalid Fraction must be Invalid Fraction");
	}
	
	@Test
	void testInvalidFractionAddAnotherInvalidFraction() {
		Fraction addFraction = InvalidFraction.of().add(InvalidFraction.of());
		assertTrue(addFraction instanceof InvalidFraction, "Invalid Fraction add Invalid Fraction must be Invalid Fraction");
	}
	
	@Test
	void testInvalidFractionAddNullFraction() {
		Fraction addFraction = InvalidFraction.of().add(NullFraction.of());
		assertTrue(addFraction instanceof InvalidFraction, "Invalid Fraction add Null Fraction must be Invalid Fraction");
	}
	
	@Test
	void testInvalidFractinAddValidOther() {
		Fraction addFraction = InvalidFraction.of().add(SimpleFraction.builder().numerator(1).denominator(5).build());
		assertTrue(addFraction instanceof InvalidFraction, "Invalid Fraction add Other Fraction must be Invalid Fraction");
	}
	
	@Test
	void testNullFractionAddNullFraction() {
		Fraction addFraction = NullFraction.of().add(NullFraction.of());
		assertTrue(addFraction instanceof NullFraction, "Null Fraction add Null Fraction must be Null Fraction");
	}
	
	@Test
	void testNullFractionAddValidFraction() {
		Fraction nullFraction = NullFraction.of();
		Fraction otherFraction = SimpleFraction.builder().numerator(1).denominator(5).build();
		Fraction addFraction = nullFraction.add(otherFraction);
		assertEquals(otherFraction, addFraction, "Null Fraction add other Fraction must be other");
	}
	
	@Test
	void testNullFractionAddInvalidFraction() {
		Fraction addFraction = NullFraction.of().add(InvalidFraction.builder().build());
		assertTrue(addFraction instanceof InvalidFraction, "Null Fraction add Invalid Fraction must be Invalid Fraction");
	}
	

}
