package fr.lacombe.fraction;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class ReverseFractionTest {
	@Test
	void testReverseValidFraction() {
		Fraction fraction = SimpleFraction.builder().numerator(2).denominator(5).build().inverse();
		Fraction reverse = fraction.inverse();
		assertEquals(fraction.getNumerator(), reverse.getDenominator(), "Numerator must become Denominator");
		assertEquals(fraction.getDenominator(), reverse.getNumerator(), "Denominator must become Numerator");
	}
	
	@Test
	void testReverseInvalidFractionWithZeroNumerator() {
		Fraction reverseFraction = InvalidFraction.of().inverse();
		assertTrue(reverseFraction instanceof InvalidFraction, "Reverse Invalid Fraction with zero numerator must be Invalid Fraction");
	}
	
	@Test
	void testReverseInvalidFractionWithNonZeroNumerator() {
		Fraction reverseFraction = InvalidFraction.builder().numerator(1).build().inverse();
		assertTrue(reverseFraction instanceof NullFraction, "Reverse Invalid Fraction with non zero numerator must be Null Fraction");
	}
	
	@Test
	void testReverseNullFraction() {
		Fraction nullFraction = NullFraction.of();
		Fraction reverse = nullFraction.inverse();
		assertTrue(reverse instanceof InvalidFraction, "Null Fraction must not have reverse");
	}
}
