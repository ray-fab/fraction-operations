package fr.lacombe.fraction;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class MultiplyFractionTest {
	
	@Test
	void testValidFractionMultiplyAnotherValidFraction() {
		Fraction leftFraction = SimpleFraction.builder().numerator(2).denominator(3).build();
		Fraction rigthFraction = SimpleFraction.builder().numerator(4).denominator(5).build();
		Fraction multiplyFraction = leftFraction.multiply(rigthFraction);
		assertEquals(multiplyFraction.getNumerator(), leftFraction.getNumerator()*rigthFraction.getNumerator(), "Numerator must 8");
		assertEquals(8, multiplyFraction.getNumerator());
		assertEquals(multiplyFraction.getDenominator(), leftFraction.getDenominator()*rigthFraction.getDenominator());
		assertEquals(15, multiplyFraction.getDenominator(), "Numerator must 15");
	}
	
	@Test
	void testValidFractionMultiplyAnotherValidFractionAndSimplifly() {
		Fraction leftFraction = SimpleFraction.builder().numerator(2).denominator(3).build();
		Fraction rigthFraction = SimpleFraction.builder().numerator(7).denominator(4).build();
		Fraction multiplyFraction = leftFraction.multiply(rigthFraction);
		assertNotEquals(multiplyFraction.getNumerator(), leftFraction.getNumerator()*rigthFraction.getNumerator(), "Numerator must 8");
		assertEquals(7, multiplyFraction.getNumerator(), "Numerator must 7");
		assertNotEquals(multiplyFraction.getDenominator(), leftFraction.getDenominator()*rigthFraction.getDenominator());
		assertEquals(6, multiplyFraction.getDenominator(), "Numerator must 6");
	}
	
	@Test
	void testValidFractionMultiplyInvalidFraction() {
		Fraction multiplyFraction = SimpleFraction.builder().numerator(2).denominator(3).build().multiply(InvalidFraction.of());
		assertTrue(multiplyFraction instanceof InvalidFraction, "Multiply Valid Fraction with Invalid Fraction must be Invalid Fraction");
	}
	
	@Test
	void testValidFractionMultiplyNullFraction() {
		Fraction multiplyFraction = SimpleFraction.builder().numerator(2).denominator(3).build().multiply(NullFraction.of());
		assertTrue(multiplyFraction instanceof NullFraction, "Multiply Valid Fraction with Null Fraction must be Null Fraction");
	}
	
	@Test
	void testInvalidFractionMultiplyAnotherInvalidFraction() {
		Fraction multiplyFraction = InvalidFraction.of().multiply(InvalidFraction.of());
		assertTrue(multiplyFraction instanceof InvalidFraction, "Invalid Fraction multiply Invalid must be Invalid Fraction");
	}
	
	@Test
	void testInvalidFractionMultiplyNullFraction() {
		Fraction multiplyFraction = InvalidFraction.of().multiply(NullFraction.of());
		assertTrue(multiplyFraction instanceof InvalidFraction, "Invalid Fraction multiply Null Fraction must be Invalid Fraction");
	}
	
	@Test
	void testInvalidFractionMultiplyValidFraction() {
		Fraction multiplyFraction = InvalidFraction.of().multiply(SimpleFraction.builder().numerator(2).denominator(3).build());
		assertTrue(multiplyFraction instanceof InvalidFraction, "Invalid Fraction multiply Other must be Invalid Fraction");
	}
	
	@Test
	void testNullFractionMultiplyAnotherNullFraction() {
		Fraction multiplyFraction = NullFraction.of().multiply(NullFraction.of());
		assertTrue(multiplyFraction instanceof NullFraction, "Null Fraction multiply Null Fraction must be Null Fraction");
	}
	
	@Test
	void testNullFractionMultiplyValidFraction() {
		Fraction multiplyFraction = NullFraction.of().multiply(SimpleFraction.builder().numerator(2).denominator(3).build());
		assertTrue(multiplyFraction instanceof NullFraction, "Null Fraction multiply Other must be Null Fraction");
	}
	
	@Test
	void testNullFractionMultiplyInvalidFraction() {
		Fraction multiplyFraction = NullFraction.of().multiply(InvalidFraction.of());
		assertTrue(multiplyFraction instanceof InvalidFraction, "Null Fraction multiply Invalid must be Invalid Fraction");
	}
}
