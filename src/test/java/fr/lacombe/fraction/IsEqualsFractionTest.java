package fr.lacombe.fraction;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class IsEqualsFractionTest {

	@Test
	void testIsNotEquals() {
		Fraction leftFraction = SimpleFraction.builder().numerator(2).denominator(3).build();
		Fraction rigthFraction = SimpleFraction.builder().numerator(4).denominator(5).build();
		assertFalse(leftFraction.isEquals(rigthFraction), "Left is not equals than rigth");
	}
	
	@Test
	void testIsEquals() {
		Fraction leftFraction = SimpleFraction.builder().numerator(2).denominator(3).build();
		Fraction rigthFraction = SimpleFraction.builder().numerator(2).denominator(3).build();
		assertTrue(leftFraction.isEquals(rigthFraction), "Left is equals than rigth");
		assertEquals(leftFraction, rigthFraction, "Left is equals than rigth");
	}
	
	@Test
	void testIsEqualsSimplifly() {
		Fraction leftFraction = SimpleFraction.builder().numerator(2).denominator(3).build();
		Fraction rigthFraction = SimpleFraction.builder().numerator(10).denominator(15).build();
		assertTrue(leftFraction.isEquals(rigthFraction), "Left is equals than rigth");
		assertNotEquals(leftFraction, rigthFraction, "Left is not equals than rigth");
	}
}
