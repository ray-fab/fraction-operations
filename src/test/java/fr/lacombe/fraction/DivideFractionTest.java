package fr.lacombe.fraction;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class DivideFractionTest {

	@Test
	void testValidFractionDivideAnotherValidFraction() {
		Fraction leftFraction = SimpleFraction.builder().numerator(2).denominator(3).build();
		Fraction rigthFraction = SimpleFraction.builder().numerator(5).denominator(7).build();
		Fraction multiplyFraction = leftFraction.divide(rigthFraction);
		assertEquals(multiplyFraction.getNumerator(), leftFraction.getNumerator()*rigthFraction.getDenominator());
		assertEquals(14, multiplyFraction.getNumerator(), "Numerator must 14");
		assertNotEquals(multiplyFraction.getNumerator(), rigthFraction.getNumerator()*leftFraction.getDenominator());
		assertEquals(15, multiplyFraction.getDenominator(), "Numerator must 15");
	}

	@Test
	void testValidFractionDivideAnotherValidFractionAndSimplifly() {
		Fraction leftFraction = SimpleFraction.builder().numerator(2).denominator(3).build();
		Fraction rigthFraction = SimpleFraction.builder().numerator(4).denominator(5).build();
		Fraction divideFraction = leftFraction.divide(rigthFraction);
		assertNotEquals(divideFraction.getNumerator(), leftFraction.getNumerator()*rigthFraction.getDenominator());
		assertEquals(5, divideFraction.getNumerator(), "Numerator must 5");
		assertNotEquals(divideFraction.getNumerator(), rigthFraction.getNumerator()*leftFraction.getDenominator());
		assertEquals(6, divideFraction.getDenominator(), "Numerator must 6");
	}
	
	@Test
	void testValidFractionDivideInvalidFraction() {
		Fraction divideFraction = SimpleFraction.builder().numerator(2).denominator(3).build().divide(InvalidFraction.of());
		assertTrue(divideFraction instanceof InvalidFraction, "Divide Valid Fraction with Invalid Fraction must be Inavlid Fraction");
	}
	
	@Test
	void testInvalidFractionDivideInvalidFraction() {
		Fraction divideFraction = InvalidFraction.of().divide(InvalidFraction.of());
		assertTrue(divideFraction instanceof InvalidFraction, "Invalid Fraction divide Invalid must be Invalid Fraction");
	}
	
	@Test
	void testInvalidFractionDivideNullFraction() {
		Fraction divideFraction = InvalidFraction.of().divide(NullFraction.of());
		assertTrue(divideFraction instanceof InvalidFraction, "Invalid Fraction divide Null Fraction must be Invalid Fraction");
	}
	
	@Test
	void testInvalidFractionDivideValidFraction() {
		Fraction divideFraction = InvalidFraction.of().divide(SimpleFraction.builder().numerator(2).denominator(3).build());
		assertTrue(divideFraction instanceof InvalidFraction, "Invalid Fraction divide Other must be Invalid Fraction");
	}
	

	@Test
	void testNullFractionDivideNullFraction() {
		Fraction divideFraction = NullFraction.of().divide(NullFraction.of());
		assertTrue(divideFraction instanceof InvalidFraction, "Null Fraction divide Null Fraction must be Invalid Fraction");
	}
	
	@Test
	void testNullFractionDivideOtherFraction() {
		Fraction divideFraction = NullFraction.of().divide(SimpleFraction.builder().numerator(2).denominator(3).build());
		assertTrue(divideFraction instanceof NullFraction, "Null Fraction divide Other must be Null Fraction");
	}
	
	@Test
	void testNullFractionDivideInvalidFraction() {
		Fraction divideFraction = NullFraction.of().divide(InvalidFraction.of());
		assertTrue(divideFraction instanceof InvalidFraction, "Null Fraction divide Other must be Null Fraction");
	}
}
