package fr.lacombe.fraction;

import java.math.BigInteger;
import java.util.Optional;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class SimpleFraction implements Fraction {
	
	private final int numerator;
	private final int denominator;

	@Override
	public Fraction opposite() {
		return SimpleFraction.builder().numerator(numerator * -1).denominator(denominator).build();
	}

	@Override
	public Fraction inverse() {
		return SimpleFraction.builder().numerator(denominator).denominator(numerator).build();
	}

	@Override
	public Fraction simplifly() {
		int pgcd = BigInteger.valueOf(numerator).gcd(BigInteger.valueOf(denominator)).intValue();
		return SimpleFraction.builder()
				.numerator(numerator/ pgcd)
				.denominator(denominator/pgcd)
				.build();
	}
	
	private Fraction doAdd(Fraction other) {
		return SimpleFraction.builder()
				.numerator(numerator * other.getDenominator() + denominator * other.getNumerator())
				.denominator(denominator * other.getDenominator())
				.build()
				.simplifly();
	}

	@Override
	public Fraction add(Fraction other) {
		return Optional.ofNullable(other).map(this::doAdd).orElseGet(NullFraction::of);
	}
	
	private Fraction doMultiply(Fraction other) {
		return SimpleFraction.builder()
				.numerator(numerator * other.getNumerator())
				.denominator(denominator * other.getDenominator())
				.build()
				.simplifly();
	}

	@Override
	public Fraction multiply(Fraction other) {
		return Optional.ofNullable(other).map(this::doMultiply).orElseGet(NullFraction::of);
	}
	
	@Override
	public String toString() {
		Fraction simplifly = simplifly();
		return String.format("%s/%s", simplifly.getNumerator(), simplifly.getDenominator());
	}
	
	public static class SimpleFractionBuilder {
		
		public Fraction build() {
			Fraction fraction = new SimpleFraction((denominator <0 ? -1: 1)*numerator, BigInteger.valueOf(denominator).abs().intValue());
			return Optional.of(fraction)
					.filter(Fraction::isValid)
					.map(unused -> numerator == 0? NullFraction.of(): fraction)
					.orElseGet(InvalidFraction.builder().numerator(numerator)::build);
		}
	}

}
