package fr.lacombe.fraction;

import java.util.Optional;
import java.util.function.Predicate;

public final class NullFraction extends SimpleFraction implements Fraction {

	private NullFraction(int denominator) {
		super(0, denominator);
	}
	
	private NullFraction() {
		this(1);
	}
	
	@Override
	public boolean isGreaterThan(Fraction other) {
		return Optional.ofNullable(other)
				.filter(Fraction::isValid)
				.map(super::isGreaterThan)
				.orElse(false);
	}
	
	@Override
	public boolean isLessThan(Fraction other) {
		return false;
	}

	@Override
	public Fraction inverse() {
		return InvalidFraction.builder().build();
	}

	@Override
	public Fraction simplifly() {
		return this;
	}

	@Override
	public Fraction add(Fraction other) {
		return other;
	}

	@Override
	public Fraction multiply(Fraction other) {
		return Optional.ofNullable(other)
				.filter(Predicate.not(Fraction::isValid))
				.map(InvalidFraction::of)
				.map(Fraction.class::cast)
				.orElse(this);
	}
	
	public static NullFraction of() {
		return new NullFraction();
	}
	
	public static NullFraction of(int numerator) {
		return new NullFraction(numerator);
	}
	
	@Override
	public String toString() {
		return "[Null Fraction]";
	}

}
