package fr.lacombe.fraction;

import java.util.Optional;

import lombok.Builder;
import lombok.Getter;

public final class InvalidFraction implements Fraction {
	@Getter
	private final int numerator;
	
	@Builder
	private InvalidFraction(Fraction other, int numerator) {
		this.numerator = Optional.ofNullable(other).map(Fraction::getNumerator).orElse(numerator);
	}
	
	@Override
	public boolean isGreaterThan(Fraction other) {
		return false;
	}
	
	@Override
	public boolean isLessThan(Fraction other) {
		return false;
	}

	@Override
	public double getResult() {
		throw new UnsupportedOperationException("Division by zero");
	}
	
	@Override
	public int compareTo(Fraction other) {
		return Optional.ofNullable(other).map(unused -> 0).orElse(1);
	}

	@Override
	public int getDenominator() {
		return 0;
	}

	@Override
	public Fraction opposite() {
		return this;
	}

	@Override
	public Fraction inverse() {
		return numerator!=0 ? NullFraction.of() : this;
	}

	@Override
	public Fraction simplifly() {
		return this;
	}

	@Override
	public Fraction add(Fraction other) {
		return this;
	}

	@Override
	public Fraction multiply(Fraction other) {
		return this;
	}
	
	public static InvalidFraction of() {
		return InvalidFraction.builder().build();
	}
	
	public static InvalidFraction of(Fraction other) {
		return InvalidFraction.builder().other(other).build();
	}
	
	@Override
	public String toString() {
		return "[Invalid Fraction]";
	}
}
