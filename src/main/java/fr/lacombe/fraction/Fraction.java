package fr.lacombe.fraction;

import java.util.Objects;

public interface Fraction extends Comparable<Fraction> {

	int getNumerator();

	int getDenominator();

	Fraction opposite();

	Fraction inverse();

	Fraction simplifly();

	Fraction add(Fraction other);
	
	Fraction multiply(Fraction other);

	default boolean isValid() {
		return getDenominator() != 0;
	}

	default double getResult() {
		double numerator = getNumerator();
		double denominator = getDenominator();
		return numerator / denominator;
	}

	default Fraction substract(Fraction other) {
		return add(other.opposite()).simplifly();
	}

	default Fraction divide(Fraction other) {
		return multiply(other.inverse()).simplifly();
	}

	@Override
	default int compareTo(Fraction other) {
		if (Objects.isNull(other) || !other.isValid() || !isValid())
			return -1;
		if (equals(other) || simplifly().equals(other) || other.simplifly().equals(this))
			return 0;
		return getResult() - other.getResult() > 0 ? 1 : -1;
	}

	default boolean isGreaterThan(Fraction other) {
		return compareTo(other) > 0;
	}

	default boolean isLessThan(Fraction other) {
		return compareTo(other) < 0;
	}

	default boolean isEquals(Fraction other) {
		return compareTo(other) == 0;
	}
}
